#!/bin/bash
#sh file.sh --groupname policyurn $(<pol)
aws iam create-group --group-name "$1" > output/group_details
cntr=1
aws iam attach-group-policy --policy-arn "$2" --group-name "$1"
while read var
do 
  aws iam create-user --user-name "$var" >> output/user_details
  aws iam create-login-profile --user-name "$var" --password Cl0udthat@"$cntr">>output/login_details
  cntr=$(($cntr+1)) 
  aws iam add-user-to-group --user-name "$var" --group-name "$1"
  aws iam create-access-key --user-name "$var">>output/key_details

done < user

egrep "UserName|SecretAccessKey|AccessKeyId" output/key_details > temp/accesskey.txt
cut -d: -f2 temp/accesskey.txt> temp/sec.txt
sed s'/[ ",]//g' temp/sec.txt > final.txt
python makecsv.py


